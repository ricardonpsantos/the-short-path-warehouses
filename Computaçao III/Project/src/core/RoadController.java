/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package core;

import java.util.ArrayList;

public class RoadController {
	
	private ArrayList<Road> Roads;
	
	/* Constructor: receives array's with the individual cost's of the roads */
	public RoadController(ArrayList<Road> Roads) {
		this.Roads = Roads;
	}
	
	/* get a cost according to the index in array
	 *-----------------------------------------------------------------------------------------------------*/
	public Road get(int index) {
		return Roads.get(index);
	}

	/* quantity of the costs in the array's
	 *-----------------------------------------------------------------------------------------------------*/
	public int RoadsSize() {
		return Roads.size();
	}

	/* Get a Road by RoadId 
	 ----------------------------------------------------------------------------------------------*/
	public Road getById(int id) {
		Road road = null;
		
		for (int i = 0 ; i < Roads.size(); i++) {
			road = Roads.get(i);
			if (road.getRoadId() == id) return road;
		}
		
		return null;
	}
	
	public int getNextId() {
		
		Road road= null;
		int biggerId=0;
		
		
	for(int i =0;i <Roads.size();i++) {
		
		road= Roads.get(i);
		if(road.getRoadId() > biggerId) 
			biggerId = road.getRoadId();
		
	}
	
		return biggerId+1;
	}
}
