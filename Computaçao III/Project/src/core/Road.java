/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Road
*------------------------------------------------------------------------------------------------------------*/

package core;

public class Road {
	
	private int RoadId;
	private City Origin;
	private City Destination;
	private char Group;
	private double distance;
	private double costDriver;
	private double costFuel;
	private double costPoll;
	private double time;
	private String period;
		
	/* Constructor with data */
	public Road (int id, char group, City Origin,  City Destination, double distance, double time, double costFuel, double costPoll, double costDriver, String period) {	
		this.RoadId = id;
		this.Group = group;
		this.Origin = Origin; 
		this.Destination= Destination; 
		this.distance = distance; 
		this.time = time;
		this.costFuel = costFuel;
		this.costPoll = costPoll;
		this.costDriver = costDriver;
		this.period = period;
	}
	
	/* -----------------GETTER AND SETTER -----------------------*/
	public int getRoadId() {
		return RoadId;
	}

	public void setRoadId(int roadId) {
		RoadId = roadId;
	}

	public City getOrigin() {
		return Origin;
	}

	public void setOrigin(City origin) {
		Origin = origin;
	}

	public City getDestination() {
		return Destination;
	}

	public void setDestination(City destination) {
		Destination = destination;
	}

	public char getGroup() {
		return Group;
	}

	public void setGroup(char group) {
		Group = group;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getCostDriver() {
		return costDriver;
	}

	public void setCostDriver(double costDriver) {
		this.costDriver = costDriver;
	}

	public double getCostFuel() {
		return costFuel;
	}

	public void setCostFuel(double costFuel) {
		this.costFuel = costFuel;
	}

	public double getCostPoll() {
		return costPoll;
	}

	public void setCostPoll(double costPoll) {
		this.costPoll = costPoll;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}
	 
}
