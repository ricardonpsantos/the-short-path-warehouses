/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Controller of City list (cities loaded in the system)
*------------------------------------------------------------------------------------------------------------*/

package core;

import java.util.ArrayList;

public class CitiesController {
	
	private ArrayList<City> cities;
	
	
	/* Constructor
	 *-----------------------------------------------------------------------------------------------------*/
	public CitiesController(){
		
	}
	
	/* Constructor 2: which receives an array of cities (cities loaded)
	 *-----------------------------------------------------------------------------------------------------*/
	public CitiesController(ArrayList<City> cities){
		this();
		this.cities = cities;
	}
	
	/* get a city according to the index in array
	 *-----------------------------------------------------------------------------------------------------*/
	public City get(int index) {
		return cities.get(index);
	}
	
	
	/* quantity of the cities in the array
	 *-----------------------------------------------------------------------------------------------------*/
	public int size() {
		return cities.size();
	}
	
	
	/* add a city to the array
	 *-----------------------------------------------------------------------------------------------------*/
	public void add(City city) {
		cities.add(city);
	}
	
	
	/* delete a city from the array
	 *-----------------------------------------------------------------------------------------------------*/
	public void delete(int index) {
		cities.remove(index);
	}
	
	public ArrayList<City> getEntireList(){
		return cities;
	}
	
	
	/* Get a city by CityId 
	 ----------------------------------------------------------------------------------------------*/
	public City getById(int id) {
		City city = null;
		
		for (int i = 0 ; i < cities.size(); i++) {
			city = cities.get(i);
			if (city.getId() == id) return city;
		}
		
		return null;
	}
	
	/* Get next CityId (Bigger CityId to be used by a new city)
	 ----------------------------------------------------------------------------------------------*/
	public int getNextId() {
		City city = null;
		int biggerId = 0;
		
		for (int i = 0 ; i < cities.size(); i++) {
			city = cities.get(i);
			if (city.getId() > biggerId ) biggerId = city.getId();
		}
		
		return biggerId + 1;
		
		// Remark: this code can be optimized.
	}

	public City getByName(String cityName) {
		City city = null;
		
		for (int i = 0 ; i < cities.size(); i++) {
			city = cities.get(i);
			if (city.getName().equals(cityName) ) return city;
		}
		return null;
	}
}
