/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package core;

import java.util.ArrayList;

public class WarehouseController {
	
	private ArrayList<Warehouse> warehouses;
	
	public  WarehouseController (ArrayList<Warehouse> warehouses) {
		
		this.warehouses = warehouses;
	}
	
	/* Get a city by CityId 
	 ----------------------------------------------------------------------------------------------*/
	public Warehouse getById(int id) {
		Warehouse w = null;
		
		for (int i = 0 ; i < warehouses.size(); i++) {
			w = warehouses.get(i);
			if (w.getId() == id) return w;
		}	
		return null;
	}

	public int getNextId() {
		int maxId = 0;
		
		for (int i = 0 ; i < warehouses.size(); i++) {
			Warehouse w = warehouses.get(i);
			if (w.getId() > maxId) {
				maxId = w.getId();
			}
		}
		return maxId + 1;
	}

}
