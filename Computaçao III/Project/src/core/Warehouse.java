/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Warehouse
*------------------------------------------------------------------------------------------------------------*/

package core;

public class Warehouse {
	
	/* Member variables
	 ----------------------------------------------------------------------------------------------*/
	private int warehouseId;
	private String warehouseName;
	private City city;
	
	/* Constructor
	 ----------------------------------------------------------------------------------------------*/
	public Warehouse(int warehouseId, String warehouseName, City city) {
		this.warehouseId = warehouseId;
		this.warehouseName = warehouseName;
		this.city = city;
	}
	
	/* Getters and Setters
	 ----------------------------------------------------------------------------------------------*/

	public int getId() {
		return warehouseId;
	}

	public void setId(int warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getName() {
		return warehouseName;
	}

	public void setName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}
