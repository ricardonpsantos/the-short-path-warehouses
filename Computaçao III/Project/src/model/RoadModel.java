/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import core.Road;

public class RoadModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;

	private final ArrayList<Road> roads;
	 
	// the names of the columns
	private final String[] columnNames = new String[] {
		"Road Id",
		"Road City Origin",
		"Road City Destination",
		"Road Group",
		"Road Fuel Cost",
		"Road Poll Cost",
		"Road Driver Cost",
		"Road Distance",
		"Road Time [hours]"
	};
	
	// data type of the columns
	@SuppressWarnings("rawtypes")
	private final Class[] columnClass = new Class[] {
		Integer.class,
		String.class,
		String.class,
		String.class,
		Double.class,
		Double.class,
		Double.class,
		Double.class,
		Integer.class		
	};
	
	public RoadModel(ArrayList<Road> roads) {
		this.roads = roads; 	
	}
	
	// Method to return the column name (Table) according to the column (index)
    @Override
	public String getColumnName(int column){
	        return columnNames[column];//proper return
	}
    
    // Method to return the "data type" according to the column (index) 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];// proper return
    }
	
	@Override
	public int getRowCount() {
		return roads.size();//proper return
	}

	@Override
	public int getColumnCount() {
		return columnNames.length; //proper return
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Road row = roads.get(rowIndex);
		
		//IFs
		if (columnIndex == 0) return row.getRoadId();
		if (columnIndex == 1) return row.getOrigin().getName();
		if (columnIndex == 2) return row.getDestination().getName();
		if (columnIndex == 3) return row.getGroup();
		if (columnIndex == 4) return row.getCostFuel();
		if (columnIndex == 5) return row.getCostPoll();
		if (columnIndex == 6) return row.getCostDriver();
		if (columnIndex == 7) return row.getDistance();
		if (columnIndex == 8) return row.getTime();
		
		return null;
	}

}