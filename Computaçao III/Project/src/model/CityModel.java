/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	City Model - to be loaded in JTable (Visual component)
*------------------------------------------------------------------------------------------------------------*/

package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import core.City;

public class CityModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;

	// data = arrayList of cities (each record to be presented in the table
	private final ArrayList<City> cities;

	// the names of the columns
	private final String[] columnNames = new String[] {
			"City Id", 
			"City name", 
			"District", 
			"Has warehouse", 
			"Has roads", 
			"Roads"
	};
	
	// data type of the columns
	@SuppressWarnings("rawtypes")
	private final Class[] columnClass = new Class[] {
			Integer.class, 
			String.class, 
			String.class, 
			Boolean.class, 
			Boolean.class, 
			Integer.class		
	};
	
	// Constructor
	public CityModel(ArrayList<City> cities) {
		this.cities = cities;
	}
	
	
	// Method to return the column name (Table) according to the column (index)
    @Override
	public String getColumnName(int column){
	        return columnNames[column];//columnsNames[column];
	}
    
    // Method to return the "data type" according to the column (index) 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
	
    // return the number of lines (records in the table)
	@Override
	public int getRowCount() {
		return cities.size();
	}
	
	// return the number of the columns
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
	// get each cell value according to rwoIndex and columnIndex
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		City row = cities.get(rowIndex);

		// ID, Question,Alternative1,Alternative2,Alternative3,Alternative4,Answer,Explanation
		if (columnIndex == 0) return row.getId();
		if (columnIndex == 1) return row.getName();
		if (columnIndex == 2) return row.getDistrict();
		if (columnIndex == 3) return row.hasWarehouse();
		if (columnIndex == 4) return row.hasRoad();
		if (columnIndex == 5) return row.getNumberOfRoads();
		return null;	
	}

} // class
