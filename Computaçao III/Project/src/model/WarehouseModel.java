/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import core.Warehouse;

public class WarehouseModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;

	private final ArrayList<Warehouse> warehouses;
	
	// the names of the columns
	private final String[] columnNames = new String[] {
			"Warehouse Id", 
			"Warehouse name", 
			"Warehouse city"
	};

	// data type of the columns
	@SuppressWarnings("rawtypes")
	private final Class[] columnClass = new Class[] {
			Integer.class, 
			String.class, 
			String.class		
	};
	
	public WarehouseModel(ArrayList<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}
	
	// Method to return the column name (Table) according to the column (index)
    @Override
	public String getColumnName(int column){
	        return columnNames[column];//columnsNames[column];
	}
    
    // Method to return the "data type" according to the column (index) 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
	
	@Override
	public int getRowCount() {
		return warehouses.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Warehouse row = warehouses.get(rowIndex);
		
		// ID, Question,Alternative1,Alternative2,Alternative3,Alternative4,Answer,Explanation
		if (columnIndex == 0) return row.getId();
		if (columnIndex == 1) return row.getName();
		if (columnIndex == 2) return row.getCity().getName();
		
		return null;
	}

}
