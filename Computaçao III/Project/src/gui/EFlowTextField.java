/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package gui;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class EFlowTextField extends JTextField{
	
	private static final long serialVersionUID = -3084414507167976257L;
	
	private Border defaultBorder;

	public EFlowTextField() {
		
		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				defaultBorder = getBorder();
				setBorder(new LineBorder(new Color(216, 54, 97)));//pink/red
			}
			
			@Override
			public void focusLost(FocusEvent e) {
				setBorder(defaultBorder);
			}
		});
	
	}

}