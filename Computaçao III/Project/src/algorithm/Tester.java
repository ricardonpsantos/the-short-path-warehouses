/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package algorithm;

import algorithm.cost.AlgorithmWeight;
import algorithm.cost.AlgorithmWeightByCost;
import core.CitiesController;
import core.City;
import core.Road;
import core.Warehouse;
import csv.CSVReader;
import parser.CityParser;
import parser.RoadParser;
import parser.WarehouseParser;

import java.util.ArrayList;
import java.util.List;

// Ficheiro de testes
public class Tester {
    public static void main(String[] args) {
        //
        //                       O CODIGO ABAIXO E SO PARA TESTAR
        //  \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
        CSVReader cityReader = new CSVReader();
        CSVReader warehouseReader = new CSVReader();
        CSVReader roadReader = new CSVReader();


        ArrayList<String> citiesRecords = cityReader.load("/src/data/city.csv");
        ArrayList<City> cities = CityParser.toCityObjects(citiesRecords);
        CitiesController citiesController = new CitiesController(cities);

        ArrayList<String> warehouseRecords = warehouseReader.load("/src/data/warehouse.csv");
        WarehouseParser warehouseParser = new WarehouseParser(citiesController);
        ArrayList<Warehouse> warehouses = warehouseParser.toWarehouseObjects(warehouseRecords);

        ArrayList<String> roadsRecords = roadReader.load("/src/data/road.csv");

        // 2. Parse Array of <String> to Array <Road>
        RoadParser roadParser = new RoadParser(citiesController);
        ArrayList<Road> roads = roadParser.toRoadObjects(roadsRecords);

        City source = cities.stream()
                .filter( x -> x.getName().equalsIgnoreCase("amadora"))
                .findFirst()
                .get();
        City destination = cities.stream()
                .filter( x -> x.getName().equalsIgnoreCase("chaves"))
                .findFirst()
                .get();
        // /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
        //                       O CODIGO ACIMA E SO PARA TESTAR
        //




        // Exemplo de como chamar o algoritmo e como escolher o custo correcto
        AlgorithmWeight costWeight;
//        costWeight = new AlgorithmWeightByDistance();
//        costWeight = new AlgorithmWeightByCost();
        costWeight = new AlgorithmWeightByCost();
        Algorithm algorithm = new Algorithm(cities);
        AlgorithmResult result = algorithm.shortestPath(source, costWeight);

        // Exemplo de como mostrar o caminho a percorrer para aquele custo
        List<City> paths = result.getPaths(destination);
        double costs     = result.getCost(destination);

        // Codigo para visualisar na consola
        System.out.println("       Total Cost: " +costs);
        System.out.println("Visiting N Cities: " + paths.size());
        System.out.println("            Paths:");
        for( int i = 0; i < paths.size(); i++ ){
            if( i != 0 ){
                System.out.print(" -> ");
            }
            System.out.print(paths.get(i).getName());
        }
        System.out.println();
    }
}
