/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package algorithm.cost;

import core.Road;

public class AlgorithmWeightByCost implements AlgorithmWeight {

	@Override
	public double computeCost(Road road) {
		return road.getCostDriver() + road.getCostFuel() + road.getCostPoll();
	}

}
