/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package algorithm;

import core.City;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmResult {

    private final List<City> cities;
    private final double[]   costs;
    private final int[]      path;

    public AlgorithmResult(List<City> cities, double[] costs, int[] path) {
        this.cities = cities;
        this.costs  = costs;
        this.path   = path;
    }

    public ArrayList<City> getPaths(City destination){
        ArrayList<City> result = new ArrayList<>();
        int cityIdx = this.getIndexForCity(destination);

        while( cityIdx != -1 ){
            result.add(0, this.getCityForIndex(cityIdx));
            cityIdx = path[cityIdx];
        }
        return result;
    }

    public double getCost( City destination ){
        int cityIdx = this.getIndexForCity(destination);
        return this.costs[cityIdx];
    }

    private int getIndexForCity(City city){
        return cities.indexOf(city);
    }

    private City getCityForIndex(int cityIdx){
        return cities.get(cityIdx);
    }
}
