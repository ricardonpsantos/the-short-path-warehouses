/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package algorithm;

import java.util.*;

import algorithm.cost.AlgorithmWeight;
import core.City;
import core.Road;

public class Algorithm {

    private final List<City> cities;

    public Algorithm(List<City> cities){
        this.cities = cities;
    }

    /**
     * Function used to return the object index in the city of lists.
     * For example:
     *    ....
     *   [ 10 ] -> Amadora/Lisboa
     *   [ 11 ] -> Loures/Lisboa
     *   [ 12 ] -> Sintra/Lisboa
     *   [ 13 ] -> Lourinha/Lisboa
     *    ....
     *
     *    x = getIndexForCity( Amadora ) <=>
     *    x = 10
     */
    private int getIndexForCity(City source) {
        return this.cities.indexOf(source);
    }

    private City getCityForIndex(int idx) {
        return this.cities.get(idx);
    }
    /**
     * Returns Index of the city with the lowest cost of unvisited cities
     */
    private int getLowestCostCityIdx(List<Integer> unvisitedIdx, double[] distances){
        int minIdx = unvisitedIdx.get(0);
        for( int i = 1; i < unvisitedIdx.size(); i++ ){
            int idx = unvisitedIdx.get(i);
            if ( distances[idx] < distances[minIdx] ){
                minIdx = idx;
            }
        }
        return minIdx;
    }

    private AlgorithmResult dikjstra(int sourceIdx, AlgorithmWeight costWeight) {
        // Keep track of all visited nodes
        List<Integer> visitedIdx   = new ArrayList<>();
        // Keep track of all nodes discovered and not visited yet
        List<Integer> unvisitedIdx = new ArrayList<>();

        // Contains the cost computed after applying the algorithm
        double[] costs = new double[this.cities.size()];
        // Contains the indexes of the SPT (Shortest Path Tree)
        int[]    paths     = new int[this.cities.size()];

        // Initialize all auxiliar structures:
        //    Distances = MAXIMUM VALUE POSSIBLE
        //    Paths     = cities pointing to themselves
        for( int i = 0; i < costs.length; i++){
            costs[i] = Double.MAX_VALUE;
            paths    [i] = i;
        }

        // The source city will start with cost 0 and is the root city (-1)
        costs    [sourceIdx] =  0;
        paths    [sourceIdx] = -1;
        unvisitedIdx.add(sourceIdx);

        while(!unvisitedIdx.isEmpty()){
            // Get the unvisited not with lower cost
            int curCityIdx = getLowestCostCityIdx(unvisitedIdx, costs);
            // Remove the city from the list of unvisited and mark it as visited
            unvisitedIdx.remove(new Integer(curCityIdx));
            visitedIdx.add(curCityIdx);

            // For all Adjacencies do:
            List<Road> adjacentRoads = this.getCityForIndex(curCityIdx).getAdjacencies();
            for( int i = 0; i < adjacentRoads.size(); i++ ){
                Road road   = adjacentRoads.get(i);
                int cityIdx = getIndexForCity(road.getDestination());
                // If the adjacency was already visited, then skip
                if (visitedIdx.contains(cityIdx)){
                    continue;
                }
                // computes the from the current city to the adjacency
                // If the cost is lower, set the new cost and set the new parent
                double curCityCost = costWeight.computeCost(road) + costs[curCityIdx];
                if ( curCityCost < costs[cityIdx]){
                    costs[cityIdx] = curCityCost;
                    paths[cityIdx] = curCityIdx;
                }
                unvisitedIdx.add(cityIdx);
            }
        }
        return new AlgorithmResult(cities, costs, paths);
    }

    public AlgorithmResult shortestPath(City source, AlgorithmWeight costWeight) {
        int sourceIndx = getIndexForCity(source);
        return dikjstra(sourceIndx, costWeight);
    }
}
