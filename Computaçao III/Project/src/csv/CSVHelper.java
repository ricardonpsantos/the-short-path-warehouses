/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package csv;

public final class CSVHelper {
	 public final static String NEW_LINE_SEPARATOR = "\n";
	 public final static String FIELD_SEPARATOR = ",";

}
