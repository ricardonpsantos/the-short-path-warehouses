/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Debugger
*------------------------------------------------------------------------------------------------------------*/

package app;

import java.util.ArrayList;

import core.City;
import core.Road;
import core.Warehouse;
import csv.CSVReader;

public final class Debugger {
	
	/* Print the file information and the content with no parse, only line (string) with comma separator
	 ----------------------------------------------------------------------------------------------*/	
	public static void printFileContent(CSVReader reader, ArrayList<String> allRecords) {
		// Debug
		System.out.println("\n *** PATH       : " + reader.getPath() );
		System.out.println("\n *** FULL PATH  : " + reader.getFullPathFileName() );
		System.out.println("\n *** HEADER     : " + reader.getHeader());
		
		System.out.println("\n *** LOADED CONTENT : ARRAYLIST OF STRING, BEFORE PARSING *** \n");
				
		// File Content 
		for (int i = 0; i < allRecords.size(); i++) {
			System.out.println(allRecords.get(i));
		}
	}
	
	/* Print cities objects
	 ----------------------------------------------------------------------------------------------*/
	public static void printCities(ArrayList<City> cities) {
		System.out.println("\n *** CITY OBJECTS - PARSED *** \n");
		for(int i = 0; i < cities.size(); i++ ) {
			City c = cities.get(i);
			System.out.println("-------------------------------------------------------------------------------------------------");
			System.out.println("ID       : " + c.getId() + " --- name        : "  +  c.getName());
			System.out.println("District : " + c.getDistrict() + " --- hasWarehouse: "  +  c.hasWarehouse() );
		}
	}
	
	/* Print cities objects
	 ----------------------------------------------------------------------------------------------*/
	public static void printWarehouses(ArrayList<Warehouse> warehouses) {
		System.out.println("\n *** WAREHOUSE OBJECTS - PARSED *** \n");
		for(int i = 0; i < warehouses.size(); i++ ) {
			Warehouse w = warehouses.get(i);
			System.out.println("-------------------------------------------------------------------------------------------------");
			System.out.println("ID       : " + w.getId() + " --- name        : "  +  w.getName());
			System.out.println("City Name       : " + w.getCity().getName() );
		}
	}

	public static void printRoads(ArrayList<Road> roads) {
		System.out.println("\n *** ROAD OBJECTS - PARSED *** \n");
		for(int i = 0; i < roads.size(); i++ ) {
			Road r = roads.get(i);
			System.out.println("-------------------------------------------------------------------------------------------------");
			System.out.println("ID       : " + r.getRoadId() + " --- group        : "  +  r.getGroup());
			System.out.println("Origin City Name       : " + r.getOrigin().getName() );
			System.out.println("Destination City Name       : " + r.getDestination().getName() );
		}
	}

}
