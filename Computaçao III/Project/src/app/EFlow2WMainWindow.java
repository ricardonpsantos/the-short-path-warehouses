/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Main window of the application
*------------------------------------------------------------------------------------------------------------*/
package app;

import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.SwingConstants;

import com.sun.xml.internal.ws.db.glassfish.BridgeWrapper;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

import algorithm.Algorithm;
import algorithm.AlgorithmResult;
import algorithm.cost.AlgorithmWeight;
import algorithm.cost.AlgorithmWeightByDistance;
import algorithm.cost.AlgorithmWeightByCost;
import algorithm.cost.AlgorithmWeightByTime;
import core.CitiesController;
import core.City;
import core.Road;
import core.RoadController;
import core.Warehouse;
import core.WarehouseController;
import csv.CSVReader;
import csv.CSVWriter;
import gui.EFlowTextField;
import model.CityModel;
import model.RoadModel;
import model.WarehouseModel;
import parser.CityParser;
import parser.RoadParser;
import parser.WarehouseParser;

import javax.swing.JTabbedPane;
import java.awt.GridLayout;
import java.awt.SystemColor;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class EFlow2WMainWindow {
	
	private static final String CRITERIA_SELECT_THE_CRITERIA = "<Select the criteria>";
	private static final String CRITERIA_TIME = "Time";
	private static final String CRITERIA_COST = "Cost";
	private static final String CRITERIA_DISTANCE = "Distance";

	/* MEMBER VARIABLE and visual components
	 * (1) The visual components which will be declared member variables will be 
	 * visible in any method of this class
	 * 		Remark: keep the instantiation in the initialize method or in another method, according
	 * 				to the logic of your code.  
	 ----------------------------------------------------------------------------------------------*/
	private JFrame frame;

	// LOAD / SAVE
	private EFlowTextField textCityCSV;
	private EFlowTextField textWarehouseCSV;
	private EFlowTextField textRoadCSV;
	private JLabel lblRoadsLoadedLabel;
	private JLabel lblWarehouseLoadedLabel;
	private JLabel lblCitiesLoadedLabel;
	private JLabel lblLoadStatusValue;
	private JLabel lblCitiesLoadedValue;
	private JLabel lblWarehouseLoadedValue;
	private JLabel lblRoadsLoadedValue;
	private JButton btnSave;
	private JLabel lblWarehouseIdValue; 
	
	//Body
	private JTabbedPane tabPaneBody;
	
	//City view
	private GridBagLayout gbl_panelCityTab;
	private JLabel lblCityIdValue;
	private JPanel panelCityDetail;
	private JPanel panelCityTable;
	private JTextField textCityName;
	private JTextField textCityDistrict;
	private JCheckBox chckbxHasWarehouse;
	private JCheckBox chckbxHasRoads;
	private JButton btnDeleteCity;
	private JPanel panelCityToolbar;
	private ArrayList<City> cities;
	private CitiesController citiesController;
	private City currentCity;
	private JTable tableCity;
	
	// Warehouse view
	private JTextField textWarehouseName;
	private JPanel panelWarehouseTable;
	private JButton btnAddWarehouse;
	private JButton btnDeleteWarehouse;
	private JTextField textWarehouseCity;
	private JTable tableWarehouse;
	private JTable tableRoad;
	private ArrayList<Warehouse> warehouses;
	private WarehouseController warehouseController;
	private Warehouse currentWarehouse;
	private JPanel panelWarehouseDetail;
	private JPanel panelWarehouseToolbar;
	
	// Road
	private JPanel panelRoadTable;
	private JPanel panelRoadDetail;
	private JLabel lblRoadIdValue;
	private JTextField textRoadCityOrigin;
	private JTextField textRoadAdjacency;
	private JTextField textRoadDuration;
	private JTextField textRoadDistance;
	private JTextField textRoadDriverCost;
	private JTextField textRoadTollCost;
	private JTextField textRoadFuelCost;
	private ArrayList<Road> roads;
	private RoadController roadController;
	private Road currentRoad;
	private JButton btnDeleteRoad;
	private JPanel panelRoadToolbar;
	
	// Search view
	private JComboBox<String> cboxCityOrigin;
	private JComboBox<String> cboxSearchCriteria;
	private JComboBox<String> cboxCityDestinantion;
	private JTable tableSearch;

	// READERS
	private CSVReader cityReader;
	private CSVReader warehouseReader;
	private CSVReader roadReader;
	
	// RECORDS
	private ArrayList<String> citiesRecords;
	private ArrayList<String> warehouseRecords;
	private ArrayList<String> roadsRecords;
	
	private boolean flagAddWarehouse = false;
	private boolean flagAddCity = false;
	private boolean flagAddRoad = false;
	

	/*
	 MAIN method: to create and launch the window 
	 ----------------------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EFlow2WMainWindow window = new EFlow2WMainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/*
	 CONSTRUCTOR 
	 ----------------------------------------------------------------------------------------------*/
	public EFlow2WMainWindow() {
		initialize();
		hideLoadStatusComponents();
		
		hideCityToolbar();
		hideCityDetails();
		
		hideWarehouseDetails();
		hideWarehouseToolbar();
		
		hideRoadDetails();
		hideRoadToolbar();
		
	}

	/*
	 INITIALIZE in this method all visual components are initialized. 

	 This method is automatically created and updated by modifications performed in the design tab.
	 You also can change the code directly in the statements of this method.
	 ----------------------------------------------------------------------------------------------*/
	private void initialize() {
		/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		   FRAME
		  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
		
		// FRAME = the window container, this is the root component (base) 
		// of the hierarchy structure of visual components.
		frame = new JFrame();
		frame.setBounds(50, 50, 950, 750);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// frame's layout ("grid bag layout") configuration
		GridBagLayout gridBagLayout = new GridBagLayout();
		//This field holds the overrides to the column minimum width. 
		//If this field is non-null the values are applied to the gridbag after all of the minimum columns widths have been calculated. 
		//If columnWidths has more elements than the number of columns, columns are added to the gridbag to match the number of elements in columnWidth.
		gridBagLayout.columnWidths = new int[] {250, 0, 0};
		//This field holds the overrides to the row minimum heights. 
		//If this field is non-null the values are applied to the gridbag after all of the minimum row heights have been calculated. 
		//If rowHeights has more elements than the number of rows, rows are added to the gridbag to match the number of elements in rowHeights.
		gridBagLayout.rowHeights = new int[] {0, 0};
		//This field holds the overrides to the column weights. 
		//If this field is non-null the values are applied to the gridbag after all of the columns weights have been calculated. 
		//If columnWeights[i] > weight for column i, then column i is assigned the weight in columnWeights[i]. 
		//If columnWeights has more elements than the number of columns,the excess elements are ignored - they do not cause more columns to be created
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0};
		frame.getContentPane().setLayout(gridBagLayout);
		
		
		/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		   CONFIG PANEL - RIGHT SIDE
		  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
		
		// PANEL CONFIG (Right side of the app)
		JPanel panelConfig = new JPanel();
		panelConfig.setBackground(new Color(50, 205, 50));
		panelConfig.setLayout(null);
		
		// PANEL CONFIG  grid bag constraints
		GridBagConstraints gbc_panelConfig = new GridBagConstraints();
		gbc_panelConfig.insets = new Insets(0, 0, 0, 0);
		gbc_panelConfig.fill = GridBagConstraints.BOTH;
		gbc_panelConfig.gridx = 0;
		gbc_panelConfig.gridy = 0;
		frame.getContentPane().add(panelConfig, gbc_panelConfig);
		
		// APP LOGO
		JLabel lblNewLabel = new JLabel("   Cargo Flow Systems");
		lblNewLabel.setFont(new Font("Avenir", Font.BOLD, 14));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setIcon(new ImageIcon(EFlow2WMainWindow.class.getResource("/img/eflow-tranparent-white_1.png")));
		lblNewLabel.setBounds(6, 16, 235, 65);
		panelConfig.add(lblNewLabel);
		
		// LABEL Config
		JLabel lblConfig = new JLabel("Configuration");
		lblConfig.setForeground(Color.WHITE);
		lblConfig.setFont(new Font("Avenir", Font.ITALIC, 11));
		lblConfig.setBounds(16, 107, 223, 16);
		panelConfig.add(lblConfig);
		
		// LABEL City CSV
		JLabel lblCityCSV = new JLabel("City CSV Filename");
		lblCityCSV.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblCityCSV.setForeground(Color.WHITE);
		lblCityCSV.setBounds(16, 135, 201, 16);
		panelConfig.add(lblCityCSV);
		
		// LABEL Warehouse CSV
		JLabel lblWarehouseCSV = new JLabel("Warehouse CSV Filename");
		lblWarehouseCSV.setForeground(Color.WHITE);
		lblWarehouseCSV.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblWarehouseCSV.setBounds(16, 192, 201, 16);
		panelConfig.add(lblWarehouseCSV);
		
		// LABEL Road CSV
		JLabel lblRoadsSCV = new JLabel("Road CSV Filename");
		lblRoadsSCV.setForeground(Color.WHITE);
		lblRoadsSCV.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblRoadsSCV.setBounds(16, 249, 201, 16);
		panelConfig.add(lblRoadsSCV);
		
		// TEXT FIELD City CSV
		textCityCSV = new EFlowTextField();
		textCityCSV.setBounds(13, 154, 226, 26);
		textCityCSV.setText("/src/data/city.csv");
		panelConfig.add(textCityCSV);
		textCityCSV.setColumns(10);
		
		// TEXT FIELD Warehouse CSV
		textWarehouseCSV = new EFlowTextField();
		textWarehouseCSV.setColumns(10);
		textWarehouseCSV.setBounds(13, 211, 226, 26);
		textWarehouseCSV.setText("/src/data/warehouse.csv");
		panelConfig.add(textWarehouseCSV);
		
		// TEXT FIELD Road CSV
		textRoadCSV = new EFlowTextField();
		textRoadCSV.setColumns(10);
		textRoadCSV.setBounds(13, 266, 226, 26);
		textRoadCSV.setText("/src/data/road.csv");
		panelConfig.add(textRoadCSV);
		
		// BUTTON LOAD DATA
		JButton btnLoadData = new JButton("Load Data");
		btnLoadData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//call method load data
				loadData();
			}
		});
		btnLoadData.setBounds(124, 311, 117, 29);
		panelConfig.add(btnLoadData);
		
		// LABEL Status Label
		JLabel lblLoadStatusLabel = new JLabel("Load status:");
		lblLoadStatusLabel.setForeground(Color.WHITE);
		lblLoadStatusLabel.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblLoadStatusLabel.setBounds(16, 359, 86, 16);
		panelConfig.add(lblLoadStatusLabel);
		

		// LABEL Cities Loaded Label
		lblCitiesLoadedLabel = new JLabel("Cities loaded");
		lblCitiesLoadedLabel.setForeground(Color.WHITE);
		lblCitiesLoadedLabel.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblCitiesLoadedLabel.setBounds(57, 387, 160, 16);
		panelConfig.add(lblCitiesLoadedLabel);
		

		// LABEL Warehouse Loaded Label
		lblWarehouseLoadedLabel = new JLabel("Warehouses loaded");
		lblWarehouseLoadedLabel.setForeground(Color.WHITE);
		lblWarehouseLoadedLabel.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblWarehouseLoadedLabel.setBounds(57, 415, 160, 16);
		panelConfig.add(lblWarehouseLoadedLabel);
		
		
		// LABEL Roads Loaded Label
		lblRoadsLoadedLabel = new JLabel("Roads loaded");
		lblRoadsLoadedLabel.setForeground(Color.WHITE);
		lblRoadsLoadedLabel.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblRoadsLoadedLabel.setBounds(57, 443, 160, 16);
		panelConfig.add(lblRoadsLoadedLabel);
		

		// LABEL Status Value
		lblLoadStatusValue = new JLabel("NOT LOADED");
		//label.setForeground(Color.GREEN);
		lblLoadStatusValue.setForeground(Color.RED);
		lblLoadStatusValue.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblLoadStatusValue.setBounds(101, 359, 140, 16);
		panelConfig.add(lblLoadStatusValue);
		
		// LABEL Cities Loaded Value
		lblCitiesLoadedValue = new JLabel("0");
		lblCitiesLoadedValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCitiesLoadedValue.setForeground(new Color(255, 255, 255));
		lblCitiesLoadedValue.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblCitiesLoadedValue.setBounds(8, 387, 37, 16);
		panelConfig.add(lblCitiesLoadedValue);
		
		// LABEL Warehouse Loaded Value
		lblWarehouseLoadedValue = new JLabel("0");
		lblWarehouseLoadedValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblWarehouseLoadedValue.setForeground(new Color(255, 255, 255));
		lblWarehouseLoadedValue.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblWarehouseLoadedValue.setBounds(8, 414, 37, 16);
		panelConfig.add(lblWarehouseLoadedValue);
		
		// LABEL Roads Loaded Value
		lblRoadsLoadedValue = new JLabel("0");
		lblRoadsLoadedValue.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRoadsLoadedValue.setForeground(new Color(255, 255, 255));
		lblRoadsLoadedValue.setFont(new Font("Avenir", Font.PLAIN, 13));
		lblRoadsLoadedValue.setBounds(8, 442, 37, 16);
		panelConfig.add(lblRoadsLoadedValue);
		
		//JButton btnSave = new JButton("Save Data");
		btnSave = new JButton("Save Data");

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveData();
			}
		});
		btnSave.setBounds(124, 489, 113, 29);
		panelConfig.add(btnSave);
		

		/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		   BODY PANEL
		  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
		
		// PANEL BODY (where everything will be)
		JPanel panelBody = new JPanel();
		GridBagConstraints gbc_panelBody = new GridBagConstraints();
		gbc_panelBody.insets = new Insets(0, 0, 0, 0);
		gbc_panelBody.fill = GridBagConstraints.BOTH;
		gbc_panelBody.gridx = 1;
		gbc_panelBody.gridy = 0;
		frame.getContentPane().add(panelBody, gbc_panelBody);
		panelBody.setLayout(new GridLayout(0, 1, 0, 0));
		
		// TAB PANE BODY
		//JTabbedPane tabPaneBody = new JTabbedPane(JTabbedPane.TOP);
		tabPaneBody = new JTabbedPane(JTabbedPane.TOP);
		panelBody.add(tabPaneBody);
		tabPaneBody.setVisible(false);
		
		// gridBagLayout.columnWidths = new int[] {250, 0, 0};
		// gridBagLayout.rowHeights = new int[] {0, 0};
		// gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		// gridBagLayout.rowWeights = new double[]{1.0};
		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		   CITY TAB
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		//
		// ---------------------------------------------------------------------------------
		// CITY TABLE
		// ---------------------------------------------------------------------------------		
		// CITY Tool bar
		// ---------------------------------------------------------------------------------
		// CITY Details: Edit/Add fields
		// ---------------------------------------------------------------------------------
		
		// City tab (Panel)
		JPanel panelCityTab = new JPanel();
		tabPaneBody.addTab("City data", null, panelCityTab, "City data details and edition");
		// GridBagLayout gbl_panelCity = new GridBagLayout();  <** i **> moved: this component must be global.
		gbl_panelCityTab = new GridBagLayout(); 
		gbl_panelCityTab.columnWidths = new int[]{0, 0};
		gbl_panelCityTab.rowHeights = new int[] {250, 25, 0, 0};
		gbl_panelCityTab.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelCityTab.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		panelCityTab.setLayout(gbl_panelCityTab);
		
		//table of cities loaded 
		//JPanel panelCityTable = new JPanel(); ***  <** i **> moved: this component must be global.
		panelCityTable = new JPanel();
		panelCityTable.setBackground(SystemColor.scrollbar);
		GridBagConstraints gbc_panelCityTable = new GridBagConstraints();
		gbc_panelCityTable.insets = new Insets(0, 0, 5, 0);
		gbc_panelCityTable.fill = GridBagConstraints.BOTH;
		gbc_panelCityTable.gridx = 0;
		gbc_panelCityTable.gridy = 0;
		panelCityTab.add(panelCityTable, gbc_panelCityTable);
		panelCityTable.setLayout(new GridLayout(0, 1, 0, 0));
				
		tableCity = new JTable();
		panelCityTable.add(tableCity);
		
		//table too bar add, edit, delete
		//JPanel panelCityToolbar = new JPanel(); //  <** i **> moved: this component must be global.
		panelCityToolbar = new JPanel();
		panelCityToolbar.setBackground(SystemColor.window);
		GridBagConstraints gbc_panelCityToolbar = new GridBagConstraints();
		gbc_panelCityToolbar.insets = new Insets(0, 0, 5, 0);
		gbc_panelCityToolbar.fill = GridBagConstraints.BOTH;
		gbc_panelCityToolbar.gridx = 0;
		gbc_panelCityToolbar.gridy = 1;
		panelCityTab.add(panelCityToolbar, gbc_panelCityToolbar);
		
		// button add city
		JButton btnAddCity = new JButton("Add city");
		btnAddCity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCity();
			}
		});
		panelCityToolbar.setLayout(new GridLayout(0, 2, 0, 0));
		panelCityToolbar.add(btnAddCity);
		
		// button delete city
		//JButton btnDeleteCity = new JButton("Delete city");
		btnDeleteCity = new JButton("Delete city"); //  <** i **> moved: this component must be global.
		btnDeleteCity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteCity();
			}
		});
		panelCityToolbar.add(btnDeleteCity);
		btnDeleteCity.setEnabled(false); // when the application initializes this button must be disabled
		
		// City detail: fields of a city
		// JPanel panelCityDetail = new JPanel();  <** i **> moved: this component must be global.
		panelCityDetail = new JPanel();
		panelCityDetail.setBackground(new Color(64, 224, 208));
		panelCityDetail.setLayout(null);
		GridBagConstraints gbc_panelCityDetail = new GridBagConstraints();
		gbc_panelCityDetail.fill = GridBagConstraints.BOTH;
		gbc_panelCityDetail.gridx = 0;
		gbc_panelCityDetail.gridy = 2;
		panelCityTab.add(panelCityDetail, gbc_panelCityDetail);
		
		JLabel lblCityName = new JLabel("City name");
		lblCityName.setBounds(23, 34, 77, 16);
		panelCityDetail.add(lblCityName);
		
		textCityName = new JTextField();
		textCityName.setBounds(135, 29, 186, 26);
		panelCityDetail.add(textCityName);
		textCityName.setColumns(10);
		
		JLabel lblDistrict = new JLabel("District");
		lblDistrict.setBounds(23, 69, 77, 16);
		panelCityDetail.add(lblDistrict);
		
		textCityDistrict = new JTextField();
		textCityDistrict.setColumns(10);
		textCityDistrict.setBounds(135, 63, 186, 26);
		panelCityDetail.add(textCityDistrict);
		
		JLabel lblCityId = new JLabel("City Id");
		lblCityId.setBounds(139, 6, 77, 16);
		panelCityDetail.add(lblCityId);
		
		//JLabel lblCityIdValue = new JLabel("0");  <** i **> moved: this component must be global.
		lblCityIdValue = new JLabel("0");
		lblCityIdValue.setForeground(new Color(128, 0, 0));
		lblCityIdValue.setBounds(215, 6, 77, 16);
		panelCityDetail.add(lblCityIdValue);
		
		//JCheckBox chckbxHasWarehouse = new JCheckBox("Has warehouse");  <** i **> moved: this component must be global.
		chckbxHasWarehouse = new JCheckBox("Has warehouse");
		chckbxHasWarehouse.setBounds(135, 96, 128, 23);
		panelCityDetail.add(chckbxHasWarehouse);
		
		//JCheckBox chckbxHasRoads = new JCheckBox("Has roads");  <** i **> moved: this component must be global.
		chckbxHasRoads = new JCheckBox("Has roads");
		chckbxHasRoads.setBounds(135, 131, 128, 23);
		panelCityDetail.add(chckbxHasRoads);
		
		JButton btnUpdateCity = new JButton("Ok");
		btnUpdateCity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateCity();
			}
		});
		btnUpdateCity.setBounds(63, 190, 117, 29);
		panelCityDetail.add(btnUpdateCity);
		
		JButton btnCancelUpdateCity = new JButton("Cancel");
		btnCancelUpdateCity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelUpdateCity();
			}
		});
		btnCancelUpdateCity.setBounds(192, 190, 117, 29);
		panelCityDetail.add(btnCancelUpdateCity);
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		   WAREHOUSE TAB
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		//
		// ---------------------------------------------------------------------------------
		// WAREHOUSE TABLE
		// ---------------------------------------------------------------------------------		
		// WAREHOUSE Tool bar
		// ---------------------------------------------------------------------------------
		// WAREHOUSE Details: Edit/Add fields
		// ---------------------------------------------------------------------------------
		
		JPanel panelWarehouseTab = new JPanel();
		tabPaneBody.addTab("Warehouse data", null, panelWarehouseTab, "City data details and edition");
		GridBagLayout gbl_panelWarehouseTab = new GridBagLayout();
		gbl_panelWarehouseTab.columnWidths = new int[]{0, 0};
		gbl_panelWarehouseTab.rowHeights = new int[] {250, 25, 0, 0};
		gbl_panelWarehouseTab.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelWarehouseTab.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		panelWarehouseTab.setLayout(gbl_panelWarehouseTab);
		
		panelWarehouseTable = new JPanel();
		GridBagConstraints gbc_panelWarehouseTable = new GridBagConstraints();
		gbc_panelWarehouseTable.insets = new Insets(0, 0, 5, 0);
		gbc_panelWarehouseTable.fill = GridBagConstraints.BOTH;
		gbc_panelWarehouseTable.gridx = 0;
		gbc_panelWarehouseTable.gridy = 0;
		panelWarehouseTab.add(panelWarehouseTable, gbc_panelWarehouseTable);
		panelWarehouseTable.setLayout(new GridLayout(1, 0, 0, 0));
		
		tableWarehouse = new JTable();
		panelWarehouseTable.add(tableWarehouse);

		
		//JPanel panelWarehouseToolbar = new JPanel();
		panelWarehouseToolbar = new JPanel();
		panelWarehouseToolbar.setBackground(new Color(255, 248, 220));
		GridBagConstraints gbc_panelWarehouseToolbar = new GridBagConstraints();
		gbc_panelWarehouseToolbar.insets = new Insets(0, 0, 5, 0);
		gbc_panelWarehouseToolbar.fill = GridBagConstraints.BOTH;
		gbc_panelWarehouseToolbar.gridx = 0;
		gbc_panelWarehouseToolbar.gridy = 1;
		panelWarehouseTab.add(panelWarehouseToolbar, gbc_panelWarehouseToolbar);
		panelWarehouseToolbar.setLayout(new GridLayout(1, 0, 0, 0));
		
		btnAddWarehouse = new JButton("Add warehouse");
		btnAddWarehouse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addWarehouse();
			}
		});
		panelWarehouseToolbar.add(btnAddWarehouse);
		
		btnDeleteWarehouse = new JButton("Delete warehouse");
		btnDeleteWarehouse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteWarehouse();
			}
		});
		btnDeleteWarehouse.setEnabled(false);
		panelWarehouseToolbar.add(btnDeleteWarehouse);
		
		//JPanel panelWarehouseDetail = new JPanel();
		panelWarehouseDetail = new JPanel();
		panelWarehouseDetail.setBackground(new Color(64, 224, 208));
		panelWarehouseDetail.setLayout(null);
		GridBagConstraints gbc_panelWarehouseDetail = new GridBagConstraints();
		gbc_panelWarehouseDetail.fill = GridBagConstraints.BOTH;
		gbc_panelWarehouseDetail.gridx = 0;
		gbc_panelWarehouseDetail.gridy = 2;
		panelWarehouseTab.add(panelWarehouseDetail, gbc_panelWarehouseDetail);
		
		JLabel lblWarehouseName = new JLabel("Warehouse name");
		lblWarehouseName.setBounds(28, 49, 129, 16);
		panelWarehouseDetail.add(lblWarehouseName);
		
		textWarehouseName = new JTextField();
		textWarehouseName.setBounds(166, 44, 130, 26);
		panelWarehouseDetail.add(textWarehouseName);
		textWarehouseName.setColumns(10);
		
		JLabel lblWarehouseCity = new JLabel("City");
		lblWarehouseCity.setBounds(28, 82, 61, 16);
		panelWarehouseDetail.add(lblWarehouseCity);
		
		textWarehouseCity = new JTextField();
		textWarehouseCity.setBounds(166, 77, 130, 26);
		panelWarehouseDetail.add(textWarehouseCity);
		textWarehouseCity.setColumns(10);
		
		JLabel lblWarehouseIdLabel = new JLabel("Warehouse Id");
		lblWarehouseIdLabel.setBounds(174, 16, 89, 16);
		panelWarehouseDetail.add(lblWarehouseIdLabel);
		
		lblWarehouseIdValue = new JLabel("?");
		lblWarehouseIdValue.setForeground(new Color(128, 0, 0));
		lblWarehouseIdValue.setBounds(271, 16, 61, 16);
		panelWarehouseDetail.add(lblWarehouseIdValue);

		JButton btnUpdateWarehouse = new JButton("Ok");
		btnUpdateWarehouse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateWarehouse();
			}
		});
		btnUpdateWarehouse.setBounds(63, 190, 117, 29);
		panelWarehouseDetail.add(btnUpdateWarehouse);

		JButton btnCancelUpdateWarehouse = new JButton("Cancel");
		btnCancelUpdateWarehouse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelUpdateWarehouse();
			}
		});
		btnCancelUpdateWarehouse.setBounds(192, 190, 117, 29);
		panelWarehouseDetail.add(btnCancelUpdateWarehouse);

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		   ROAD TAB
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		//
		// ---------------------------------------------------------------------------------
		// ROAD TABLE
		// ---------------------------------------------------------------------------------		
		// ROAD Tool bar
		// ---------------------------------------------------------------------------------
		// ROAD Details: Edit/Add fields
		// ---------------------------------------------------------------------------------
		JPanel panelRoadTab = new JPanel();
		panelRoadTab.setBackground(SystemColor.window);
		tabPaneBody.addTab("Road data", null, panelRoadTab, "City data details and edition");
		GridBagLayout gbl_panelRoadTab = new GridBagLayout();
		gbl_panelRoadTab.columnWidths = new int[]{0, 0};
		gbl_panelRoadTab.rowHeights = new int[]{250, 25, 0, 0};
		gbl_panelRoadTab.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelRoadTab.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		panelRoadTab.setLayout(gbl_panelRoadTab);
		/*.columnWidths = new int[]{0, 0};
		 *.rowHeights = new int[] {250, 25, 0, 0};
		 *.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		 *.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		 *.setLayout(gbl_panelCity); */
		
		//JPanel panelRoadTable = new JPanel();  <** i **> moved: this component must be global.
		panelRoadTable = new JPanel();
		GridBagConstraints gbc_panelRoadTable = new GridBagConstraints();
		gbc_panelRoadTable.insets = new Insets(0, 0, 5, 0);
		gbc_panelRoadTable.fill = GridBagConstraints.BOTH;
		gbc_panelRoadTable.gridx = 0;
		gbc_panelRoadTable.gridy = 0;
		panelRoadTab.add(panelRoadTable, gbc_panelRoadTable);
		panelRoadTable.setLayout(new GridLayout(1, 0, 0, 0));
		
		tableRoad = new JTable();
		panelRoadTable.add(tableRoad);
		
		// JPanel panelRoadToolbar = new JPanel();
		panelRoadToolbar = new JPanel();
		panelRoadToolbar.setBackground(new Color(255, 255, 224));
		GridBagConstraints gbc_panelRoadToolbar = new GridBagConstraints();
		gbc_panelRoadToolbar.insets = new Insets(0, 0, 5, 0);
		gbc_panelRoadToolbar.fill = GridBagConstraints.BOTH;
		gbc_panelRoadToolbar.gridx = 0;
		gbc_panelRoadToolbar.gridy = 1;
		panelRoadTab.add(panelRoadToolbar, gbc_panelRoadToolbar);
		panelRoadToolbar.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton btnAddRoad = new JButton("Add road");
		btnAddRoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addRoad();
			}
		});
		panelRoadToolbar.add(btnAddRoad);
		
		// JButton btnDelteRoad = new JButton("Delete road");
		btnDeleteRoad = new JButton("Delete road");
		btnDeleteRoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteRoad();
			}
		});
		btnDeleteRoad.setEnabled(false);
		panelRoadToolbar.add(btnDeleteRoad);
		
		//JPanel panelRoadDetail = new JPanel();
		panelRoadDetail = new JPanel();
		panelRoadDetail.setBackground(new Color(64, 224, 208));
		panelRoadDetail.setLayout(null);
		GridBagConstraints gbc_panelRoadDetail = new GridBagConstraints();
		gbc_panelRoadDetail.fill = GridBagConstraints.BOTH;
		gbc_panelRoadDetail.gridx = 0;
		gbc_panelRoadDetail.gridy = 2;
		panelRoadTab.add(panelRoadDetail, gbc_panelRoadDetail);
		
		JLabel lblRoadCityOrigin = new JLabel("City Origin");
		lblRoadCityOrigin.setBounds(30, 40, 77, 16);
		panelRoadDetail.add(lblRoadCityOrigin);
		
		JLabel lblAdjacencycity = new JLabel("Adjacency ");
		lblAdjacencycity.setBounds(30, 68, 77, 16);
		panelRoadDetail.add(lblAdjacencycity);
		
		JLabel lblDistance = new JLabel("Distance");
		lblDistance.setBounds(29, 96, 77, 16);
		panelRoadDetail.add(lblDistance);
		
		JLabel lblDuration = new JLabel("Duration");
		lblDuration.setBounds(200, 96, 77, 16);
		panelRoadDetail.add(lblDuration);
		
		JLabel lblDriverCost = new JLabel("Driver cost");
		lblDriverCost.setBounds(30, 152, 77, 16);
		panelRoadDetail.add(lblDriverCost);
		
		JLabel lblTollCost = new JLabel("Toll cost");
		lblTollCost.setBounds(201, 152, 77, 16);
		panelRoadDetail.add(lblTollCost);
		
		JLabel lblFuelCost = new JLabel("Fuel cost");
		lblFuelCost.setBounds(350, 152, 77, 16);
		panelRoadDetail.add(lblFuelCost);
		
		textRoadCityOrigin = new JTextField();
		textRoadCityOrigin.setBounds(119, 35, 372, 26);
		panelRoadDetail.add(textRoadCityOrigin);
		textRoadCityOrigin.setColumns(10);
		
		textRoadAdjacency = new JTextField();
		textRoadAdjacency.setColumns(10);
		textRoadAdjacency.setBounds(119, 63, 372, 26);
		panelRoadDetail.add(textRoadAdjacency);
		
		textRoadDuration = new JTextField();
		textRoadDuration.setColumns(10);
		textRoadDuration.setBounds(269, 91, 68, 26);
		panelRoadDetail.add(textRoadDuration);
		
		textRoadDistance = new JTextField();
		textRoadDistance.setColumns(10);
		textRoadDistance.setBounds(118, 91, 68, 26);
		panelRoadDetail.add(textRoadDistance);

		JLabel lblCosts = new JLabel("Costs");
		lblCosts.setBounds(30, 124, 77, 16);
		panelRoadDetail.add(lblCosts);

		textRoadDriverCost = new JTextField();
		textRoadDriverCost.setColumns(10);
		textRoadDriverCost.setBounds(119, 147, 68, 26);
		panelRoadDetail.add(textRoadDriverCost);

		textRoadTollCost = new JTextField();
		textRoadTollCost.setColumns(10);
		textRoadTollCost.setBounds(270, 147, 68, 26);
		panelRoadDetail.add(textRoadTollCost);
		
		textRoadFuelCost = new JTextField();
		textRoadFuelCost.setColumns(10);
		textRoadFuelCost.setBounds(423, 147, 68, 26);
		panelRoadDetail.add(textRoadFuelCost);

		JLabel lblRoadId = new JLabel("Road Id");
		lblRoadId.setBounds(139, 6, 77, 16);
		panelRoadDetail.add(lblRoadId);

		//JLabel lblCityIdValue = new JLabel("0");  <** i **> moved: this component must be global.
		lblRoadIdValue = new JLabel("0");
		lblRoadIdValue.setForeground(new Color(128, 0, 0));
		lblRoadIdValue.setBounds(215, 6, 77, 16);
		panelRoadDetail.add(lblRoadIdValue);

		JButton btnUpdateRoad = new JButton("Ok");
		btnUpdateRoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateRoad();
			}
		});
		btnUpdateRoad.setBounds(63, 190, 117, 29);
		panelRoadDetail.add(btnUpdateRoad);
		
		JButton btnCancelUpdateRoad = new JButton("Cancel");
		btnCancelUpdateRoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelUpdateRoad();
			}
		});
		btnCancelUpdateRoad.setBounds(192, 190, 117, 29);
		panelRoadDetail.add(btnCancelUpdateRoad);
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		   SEARCH TAB
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		
		JPanel panelSearchTab = new JPanel();
		tabPaneBody.addTab("Search shortest path", null, panelSearchTab, "City data details and edition");
		GridBagLayout gbl_panelSearchTab = new GridBagLayout();
		gbl_panelSearchTab.columnWidths = new int[]{0, 0};
		gbl_panelSearchTab.rowHeights = new int[]{180, 0, 0};
		gbl_panelSearchTab.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelSearchTab.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panelSearchTab.setLayout(gbl_panelSearchTab);
		
		JPanel panelSearchForm = new JPanel();
		panelSearchForm.setLayout(null);
		GridBagConstraints gbc_panelSearchForm = new GridBagConstraints();
		gbc_panelSearchForm.insets = new Insets(0, 0, 5, 0);
		gbc_panelSearchForm.fill = GridBagConstraints.BOTH;
		gbc_panelSearchForm.gridx = 0;
		gbc_panelSearchForm.gridy = 0;
		panelSearchTab.add(panelSearchForm, gbc_panelSearchForm);
		
		JLabel lblCityOrigin = new JLabel("Origin:");
		lblCityOrigin.setBounds(49, 24, 61, 16);
		panelSearchForm.add(lblCityOrigin);
		
		//JComboBox cboxOrigin = new JComboBox(); ** Moved to global
		cboxCityOrigin = new JComboBox();
		cboxCityOrigin.setBounds(168, 20, 195, 27);
		panelSearchForm.add(cboxCityOrigin);
		
		JLabel lblSearchCriteria = new JLabel("Search criteria");
		lblSearchCriteria.setBounds(49, 92, 107, 16);
		panelSearchForm.add(lblSearchCriteria);
		
		// JComboBox cboxSearchCriteria = new JComboBox(); ** Moved to be global
		cboxSearchCriteria = new JComboBox();
		cboxSearchCriteria.setModel(new DefaultComboBoxModel(new String[] {CRITERIA_SELECT_THE_CRITERIA, CRITERIA_TIME, CRITERIA_COST, CRITERIA_DISTANCE}));
		cboxSearchCriteria.setBounds(168, 86, 195, 27);
		panelSearchForm.add(cboxSearchCriteria);
		
		// Button Search
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		btnSearch.setBounds(246, 125, 117, 29);
		panelSearchForm.add(btnSearch);
		
		JLabel lblCityDestinantion = new JLabel("Destination");
		lblCityDestinantion.setBounds(49, 58, 107, 16);
		panelSearchForm.add(lblCityDestinantion);
		
		//JComboBox cboxCityDestinantion = new JComboBox();
		cboxCityDestinantion = new JComboBox();
		cboxCityDestinantion.setBounds(168, 52, 195, 27);
		panelSearchForm.add(cboxCityDestinantion);

		JPanel panelSearchResultTable = new JPanel();
		panelSearchResultTable.setBackground(new Color(224, 255, 255));
		GridBagConstraints gbc_panelSearchResult = new GridBagConstraints();
		gbc_panelRoadTable.insets = new Insets(0, 0, 5, 0);
		gbc_panelSearchResult.fill = GridBagConstraints.BOTH;
		gbc_panelSearchResult.gridx = 0;
		gbc_panelSearchResult.gridy = 1;
		panelSearchTab.add(panelSearchResultTable, gbc_panelSearchResult);
		panelSearchResultTable.setLayout(new GridLayout(1, 0, 0, 0));
//		GridBagLayout gbl_panelSearchResult = new GridBagLayout();
//		gbl_panelSearchResult.columnWidths = new int[]{0};
//		gbl_panelSearchResult.rowHeights = new int[]{0};
//		gbl_panelSearchResult.columnWeights = new double[]{Double.MIN_VALUE};
//		gbl_panelSearchResult.rowWeights = new double[]{Double.MIN_VALUE};
//		panelSearchResultTable.setLayout(gbl_panelSearchResult);

		CityModel cityModel = new CityModel(new ArrayList<>());
		tableSearch = new JTable(cityModel);
		tableRoad.setRowSelectionAllowed(false);
		panelSearchResultTable.add(new JScrollPane(tableSearch));
	}


	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
	   LOAD / SAVE   DATA
	  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
	
	/* Print the file information and the content with no parse, only line with comma separator
	 ----------------------------------------------------------------------------------------------*/
	private void loadData() {
		// Readers
		cityReader = new CSVReader();
		warehouseReader = new CSVReader();
		roadReader = new CSVReader();
		
		// show the tabs
		tabPaneBody.setVisible(true);

		// CITY
		prepareCityView();
		
		// WAREHOUSE
		prepareWarehouseView();

		// ROADS
		prepareRoadView();

		//Update load status after all loads.
		updateLoadStatus(citiesRecords.size(), warehouseRecords.size(), roadsRecords.size() );
		
		// SEARCH
		prepareSearchView();
		
	}	
	
	/* Hide load status components
	 ----------------------------------------------------------------------------------------------*/
	private void hideLoadStatusComponents() {
		
		btnSave.setVisible(false); 
		// City
		lblCitiesLoadedLabel.setVisible(false);
		lblCitiesLoadedValue.setVisible(false);
		
		// Warehouse
		lblWarehouseLoadedLabel.setVisible(false);
		lblWarehouseLoadedValue.setVisible(false);
		
		// Road
		lblRoadsLoadedLabel.setVisible(false);
		lblRoadsLoadedValue.setVisible(false);
	}
	
	/* Update and show load status components
	 ----------------------------------------------------------------------------------------------*/
	private void updateLoadStatus(Integer nCities, Integer nWarehouse, Integer nRoads) {
		// Load status
		lblLoadStatusValue.setText("Loaded!");
		lblLoadStatusValue.setForeground(new Color(255, 215, 0));
		
		// City
		lblCitiesLoadedLabel.setVisible(true);
		lblCitiesLoadedValue.setVisible(true);
		lblCitiesLoadedValue.setText(nCities.toString());
		
		// Warehouse
		lblWarehouseLoadedLabel.setVisible(true);
		lblWarehouseLoadedValue.setVisible(true);
		lblWarehouseLoadedValue.setText(nWarehouse.toString());
		
		// Road
		lblRoadsLoadedLabel.setVisible(true);
		lblRoadsLoadedValue.setVisible(true);
		lblRoadsLoadedValue.setText(nRoads.toString());
		
		btnSave.setVisible(true); 
	}
	
	private void saveData(){
		
		// 0: Instantiate City CSWWriter
		CSVWriter cityWriter = new CSVWriter(cityReader.getPath(), textCityCSV.getText() , cityReader.getHeader());
		// 1: from City to String (ArrayList<City> to ArrayList<String>)
		ArrayList<String> citiesString = CityParser.toCSVRecords(cities);
		// 2; Save city.CSV
		cityWriter.save(citiesString);
		
		
		// 0: Instantiate Warehouse CSWWriter
		CSVWriter warehouseWriter = new CSVWriter(warehouseReader.getPath(), textWarehouseCSV.getText() , warehouseReader.getHeader());
		// 1: from Warehouse to String (ArrayList<Warehouse> to ArrayList<String>)
		ArrayList<String> warehouseString = WarehouseParser.toCSVRecords(warehouses);
		// 2; Save warehouse.CSV
		warehouseWriter.save(warehouseString);

	
		// 0: Instantiate Road CSWWriter
		CSVWriter roadWriter = new CSVWriter(roadReader.getPath(), textRoadCSV.getText(), roadReader.getHeader());
		// 1: from Road to String (ArrayList<City> to ArrayList<String>)
		ArrayList<String> RoadString = RoadParser.toCSVRecords(roads);
		// 2; Save road.CSV
		roadWriter.save(RoadString);
	}
	

	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
	   CITY
	  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
	
	
	/* ALL steps to prepare the city view
	 * - Load and Parse
	 * - Prepare the table
	 ----------------------------------------------------------------------------------------------*/
	private void prepareCityView() {
		
		// 1. Load CSV File to Array of <String> 
		citiesRecords = cityReader.load(textCityCSV.getText());
		Debugger.printFileContent(cityReader, citiesRecords);

		// 2. Parse Array of <String> to Array <City>
		cities = CityParser.toCityObjects(citiesRecords);
		Debugger.printCities(cities);
		
		citiesController = new CitiesController(cities);
		
		// 3. Prepare the table view
		CityModel cityModel = new CityModel(cities);
	
		tableCity = new JTable(cityModel);
		panelCityTable.removeAll();

		// 4. After load, show the proper visual components
		showCityToolbar();
				
		// 5. Table line clicked/selected event
		// To be executed when the user clicks on any line of the city table (JTable)
		
		
		
		// city details form
		tableCity.addMouseListener(new java.awt.event.MouseAdapter(){ 
			public void mouseClicked(java.awt.event.MouseEvent e) {
				
			int row = tableCity.rowAtPoint(e.getPoint());
			int cityId = (int) tableCity.getValueAt(row, 0);

			// get the city by Id
			currentCity = citiesController.getById(cityId);

			// 6. "link" textBoxes and City Data
			lblCityIdValue.setText(currentCity.getId().toString());
			textCityName.setText(currentCity.getName());
			textCityDistrict.setText(currentCity.getDistrict());

			chckbxHasWarehouse.setSelected(currentCity.hasWarehouse()); // textCityHasWarehouse.setText(currentCity.hasWarehouse()?"Yes":"No");
			chckbxHasRoads.setSelected(currentCity.hasRoad());

			btnDeleteCity.setEnabled(true);

			showCityDetails();
			}
		});
		
		panelCityTable.add(new JScrollPane(tableCity));
		tableCity.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
	}
	
	/* Add City
	 * Button :: Add CITY clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void addCity() {
		flagAddCity = true;
		int id = citiesController.getNextId();
		lblCityIdValue.setText(Integer.toString(id));
		textCityName.setText("");
		textCityDistrict.setText("");
		
		chckbxHasWarehouse.setSelected(false);
		chckbxHasRoads.setSelected(false);
		
		// show the visual components of city details.
		showCityDetails(); // edit mode x add mode
		
		City city = new City(id, "", "", false);
		cities.add( city );//City city = new City(id, name, district, hasWarehouse);
		currentCity = citiesController.getById(id);
		
		
		//criar entrada nova na lista
		
	}
	

	/* Delete City
	 * Button :: Delete CITY clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void deleteCity() {
		
		if (currentCity != null) {
			
			// this dialog captures YES or NO (Deletion confirmation)
			int dialogResult = JOptionPane.showConfirmDialog(frame, 
					"Attention, you will delete the city: \n" + 
					currentCity.getName() + " (id = " + currentCity.getId() + 
					").\n\nDo you confirm?",
					"Warning",
					JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			
			// if YES, EXECUTE THE DELETE ACTIONS
			if (dialogResult == JOptionPane.YES_OPTION) {
				
				cities.remove(currentCity);
				tableCity.repaint();

				JOptionPane.showMessageDialog(frame,"City was deleted");
			} 
			else {
				JOptionPane.showMessageDialog(frame,"Deletion canceled by the user. No city was deleted");
			}
		} 
		else {
			JOptionPane.showMessageDialog(frame,"No city is selected in table");
		}
		
		hideCityDetails();
	}
	
	/* Show CITY details (FORM: labels and fields)
	 ----------------------------------------------------------------------------------------------*/
	private void showCityDetails() {
		panelCityDetail.setVisible(true);
	}
	
	/* Hide CITY details (FORM: labels and fields)
	 ----------------------------------------------------------------------------------------------*/
	private void hideCityDetails() {
		btnDeleteCity.setEnabled(false);
		panelCityDetail.setVisible(false);
	}
	
	/* Hide CITY tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void hideCityToolbar() {
		panelCityToolbar.setVisible(false);
	}
	
	/* Show CITY tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void showCityToolbar() {
		panelCityToolbar.setVisible(true);
	}
	
	/* Update the CITY (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void updateCity() {
		
		currentCity.setName(textCityName.getText());
				
		currentCity.setDistrict(textCityDistrict.getText());
		
		currentCity.setHasWarehouse(chckbxHasWarehouse.isSelected());
		
		tableCity.repaint();
		
		hideCityDetails();
		if(flagAddCity)
		{
			JOptionPane.showMessageDialog(frame,"City Added!");
		}
		
		else 
		{
			JOptionPane.showMessageDialog(frame,"City updated!");
		}
		
		flagAddCity = false;
		
	}
	
	/* Cancel the CITY update (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void cancelUpdateCity() {
		if( flagAddCity )
		{
			cities.remove(currentCity);
			tableCity.repaint();
		}
		
		hideCityDetails();
		JOptionPane.showMessageDialog(frame,"City data changed canceled, no chanegs will be made");
		flagAddCity = false;
	}
	
	
	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
	   WAREHOUSE
	  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
	
	/* prepare warehouse view
	 ----------------------------------------------------------------------------------------------*/
	private void prepareWarehouseView() {
		
		// 1. Load CSV File to Array of <String> 
		warehouseRecords = warehouseReader.load(textWarehouseCSV.getText());
		Debugger.printFileContent(warehouseReader, warehouseRecords);
		
		
		// 2. Parse Array of <String> to Array <Warehouse>
		WarehouseParser warehouseParser = new WarehouseParser(citiesController);
		warehouses = warehouseParser.toWarehouseObjects(warehouseRecords);
		Debugger.printWarehouses(warehouses);
		
		warehouseController = new WarehouseController(warehouses);
		// 3. Prepare the table view
		WarehouseModel warehouseModel = new WarehouseModel(warehouses); // creating a table, giving the data = model 
	    tableWarehouse = new JTable(warehouseModel);
		
		panelWarehouseTable.removeAll();
		
		// 4. After load, show the proper visual components
		showWarehouseToolbar();
		
		// 5. Table line clicked/selected event
		// To be executed when the user clicks on any line of the warehouse table (JTable)
		tableWarehouse.addMouseListener(new java.awt.event.MouseAdapter(){ 
			public void mouseClicked(java.awt.event.MouseEvent e) {
				
				int row = tableWarehouse.rowAtPoint(e.getPoint());
				int warehouseId = (int) tableWarehouse.getValueAt(row, 0);

				currentWarehouse = warehouseController.getById(warehouseId);
				
				// 6. "link" textBoxes and City Data (there are other components)
				lblWarehouseIdValue.setText(Integer.toString(currentWarehouse.getId()));
				textWarehouseName.setText(currentWarehouse.getName());
				textWarehouseCity.setText(currentWarehouse.getCity().getName());
			
				btnDeleteWarehouse.setEnabled(true);
				
				showWarehouseDetails();
			}
		});
		
		panelWarehouseTable.add(new JScrollPane(tableWarehouse));
		tableWarehouse.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
	}
	
	/* Add WAREHOUSE
	 * Button :: Add WAREHOUSE clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void addWarehouse() {
		flagAddWarehouse = true;
		// get next id and clear the textFields
		// warehouseController = new WarehouseController(warehouses);
		int nextId = warehouseController.getNextId();
		lblWarehouseIdValue.setText("" + nextId);
		textWarehouseCity.setText("");
		textWarehouseName.setText("");

		// show the visual components with details form
		showWarehouseDetails(); // edit mode x add mode
		
		Warehouse w = new Warehouse(nextId, "", new City());
		warehouses.add(w);
		currentWarehouse = warehouseController.getById(nextId);
	}
	

	/* Delete WAREHOUSE
	 * Button :: Delete WAREHOUSE clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void deleteWarehouse() {
		
		if (currentWarehouse != null) {

			// this dialog captures YES or NO (Deletion confirmation)
			int dialogResult = JOptionPane.showConfirmDialog(frame, 
					"Attention, you will delete the warehouse: \n" + 
					currentWarehouse.getName() + " (id = " + currentWarehouse.getId() + 
					").\n\nDo you confirm?",
					"Warning",
					JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			
			// if YES, EXECUTE THE DELETE ACTIONS
			if (dialogResult == JOptionPane.YES_OPTION) {
				
				warehouses.remove(currentWarehouse);
				tableWarehouse.repaint();
				
				JOptionPane.showMessageDialog(frame,"Warehouse was deleted");
				//(...)
			} 
			else { // just inform that the deletion was canceled
				JOptionPane.showMessageDialog(frame,"Deletion canceled by the user. No warehouse was deleted");
			}
		} 
		else {
			JOptionPane.showMessageDialog(frame,"No city is selected in table");
		}
		
		hideWarehouseDetails();
	}
	
	/* Hide WAREHOUSE tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void hideWarehouseToolbar() {
		panelWarehouseToolbar.setVisible(false);
	}
	
	/* Show WAREHOUSE tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void showWarehouseToolbar() {
		panelWarehouseToolbar.setVisible(true);
	}
	
	/* Show warehouse details (Form with fields)
	 ----------------------------------------------------------------------------------------------*/
	private void showWarehouseDetails() {
		panelWarehouseDetail.setVisible(true);
	}
	
	/* hide warehouse details (Form with fields)
	 ----------------------------------------------------------------------------------------------*/
	private void hideWarehouseDetails() {
		btnDeleteWarehouse.setEnabled(false);
		panelWarehouseDetail.setVisible(false);
	}
	
	/* Update the warehouse (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void updateWarehouse() {
		// UPDATE ACTION
		String cityName = textWarehouseCity.getText();
		City city =citiesController.getByName(cityName); 
		if(city == null) {
			if (flagAddWarehouse){
				warehouses.remove(currentWarehouse);
			}
			JOptionPane.showMessageDialog(frame,"City doesn't exist ! ");
			flagAddWarehouse = false;
			return;
		}

		currentWarehouse.setName(textWarehouseName.getText());
		currentWarehouse.setCity(city);
		city.setHasWarehouse(true);

		tableWarehouse.repaint();

		hideWarehouseDetails();
		if(flagAddWarehouse)
		{
			JOptionPane.showMessageDialog(frame,"Warehouse added!");
		}
		else {
			JOptionPane.showMessageDialog(frame,"Warehouse Updated!");
		}
		flagAddWarehouse = false;
	}
	
	/* Cancel the warehouse update (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void cancelUpdateWarehouse() {
		if(flagAddWarehouse)
		{
			roads.remove(currentWarehouse);
			tableWarehouse.repaint();
		}
		
		hideWarehouseDetails();
		JOptionPane.showMessageDialog(frame,"Warehouse data changed canceled, no chanegs will be made");
	}
	
	
	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
	   ROAD
	  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
	
	/* prepare road view
	 ----------------------------------------------------------------------------------------------*/
	private void prepareRoadView() {
		
		// 1. Load CSV File to Array of <String> 
		roadsRecords = roadReader.load(textRoadCSV.getText());
		Debugger.printFileContent(roadReader, roadsRecords);
		
		// 2. Parse Array of <String> to Array <Road>
		RoadParser roadParser = new RoadParser(citiesController);
		roads = roadParser.toRoadObjects(roadsRecords);
		Debugger.printRoads(roads);
		
		roadController = new RoadController(roads);
		// 3. Prepare the table view
		RoadModel roadModel = new RoadModel(roads); // creating a table, giving the data = model 
	    tableRoad = new JTable(roadModel);
	    panelRoadTable.removeAll();
	    
		// 4. After load, show the proper visual components
		showRoadToolbar();			
		
		// 5. Table line clicked/selected event
		// To be executed when the user clicks on any line of the warehouse table (JTable)
		tableRoad.addMouseListener(new java.awt.event.MouseAdapter(){ 
			public void mouseClicked(java.awt.event.MouseEvent e) {
				
				int row = tableRoad.rowAtPoint(e.getPoint());
				int roadId = (int) tableRoad.getValueAt(row, 0);

				currentRoad = roadController.getById(roadId);
				// 6. "link" textBoxes and City Data
				lblRoadIdValue.setText(Integer.toString(currentRoad.getRoadId()));
				textRoadCityOrigin.setText(currentRoad.getOrigin().getName());
				textRoadAdjacency.setText(currentRoad.getDestination().getName());
				textRoadDistance.setText(currentRoad.getDistance()+"");
				textRoadDriverCost.setText(currentRoad.getCostDriver()+"");
				textRoadDuration.setText(currentRoad.getTime()+"");
				textRoadFuelCost.setText(currentRoad.getCostFuel()+"");
				textRoadTollCost.setText(currentRoad.getCostPoll()+"");
				
				btnDeleteRoad.setEnabled(false);
				
				showRoadDetails();
			}
		});
		
		panelRoadTable.add(new JScrollPane(tableRoad));
		tableRoad.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
	}
	
	/* Add ROAD
	 * Button :: Add ROAD clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void addRoad() {
		flagAddRoad = true;
		
		int id = roadController.getNextId();

		lblRoadIdValue.setText(Integer.toString(id));
		textRoadAdjacency.setText("");
		textRoadCityOrigin.setText("");
		textRoadDistance.setText("");
		textRoadDriverCost.setText("");
		textRoadDuration.setText("");
		textRoadFuelCost.setText("");
		textRoadTollCost.setText("");

		Road r = new Road(id, ' ', new City(), new City(), 0, 0, 0, 0, 0, "DAY/NIGHT");
		roads.add(r);
		currentRoad = roadController.getById(id);

		// show the visual components with details form
		showRoadDetails(); // edit mode x add mode
	}
	

	/* Delete WAREHOUSE
	 * Button :: Delete WAREHOUSE clicked event (it will be executed when the button is clicked)
	 ----------------------------------------------------------------------------------------------*/
	private void deleteRoad() {
		
		if (currentRoad != null) {

			// this dialog captures YES or NO (Deletion confirmation)
			int dialogResult = JOptionPane.showConfirmDialog(frame, 
					"Attention, you will delete the warehouse: " + "..." + " \n\nDo you confirm?",
					"Warning",
					JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			
			// if YES, EXECUTE THE DELETE ACTIONS
			if (dialogResult == JOptionPane.YES_OPTION) {
			
				City oldAdjacencyCity = currentRoad.getDestination();
				City oldoriginCity = currentRoad.getOrigin();
				
				if(oldAdjacencyCity != null && oldoriginCity!= null )
				{
					oldAdjacencyCity.removeAdjacency(currentRoad);
					oldoriginCity.removeAdjacency(currentRoad);
				}
				
				roads.remove(currentRoad);
				tableRoad.repaint();
				
				JOptionPane.showMessageDialog(frame,"Road was deleted");
			
			} 
			else { // just inform that the deletion was canceled
				JOptionPane.showMessageDialog(frame,"Deletion canceled by the user. No Road was deleted");
			}
		} 
		else {
			JOptionPane.showMessageDialog(frame,"No Road is selected in table");
		}
		
		hideRoadDetails();
	}
	
	/* Hide ROAD tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void hideRoadToolbar() {
		panelRoadToolbar.setVisible(false);
	}
	
	/* Show ROAD tool bar (buttons: add and delete)
	 ----------------------------------------------------------------------------------------------*/
	private void showRoadToolbar() {
		panelRoadToolbar.setVisible(true);
	}
	
	/* Show warehouse details (Form with fields)
	 ----------------------------------------------------------------------------------------------*/
	private void showRoadDetails() {
		btnDeleteRoad.setEnabled(true);
		panelRoadDetail.setVisible(true);
	}
	
	/* hide warehouse details (Form with fields)
	 ----------------------------------------------------------------------------------------------*/
	private void hideRoadDetails() {
		panelRoadDetail.setVisible(false);
	}
	
	/* Update the road (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void updateRoad() {
		City adjacencyCity = citiesController.getByName(textRoadAdjacency.getText());
		City originCity = citiesController.getByName(textRoadCityOrigin.getText());

		if( adjacencyCity == null || originCity == null ){
			if (flagAddRoad){
				roads.remove(currentRoad);
			}
			JOptionPane.showMessageDialog(frame,"Adjacency city or Origin city doesn't exist");

			tableRoad.repaint();
			flagAddRoad = false;
			return;
		}

		currentRoad.setDistance(Double.parseDouble(textRoadDistance.getText()));
		currentRoad.setCostDriver(Double.parseDouble(textRoadDriverCost.getText()));
		currentRoad.setTime(Double.parseDouble(textRoadDuration.getText()));
		currentRoad.setCostFuel(Double.parseDouble(textRoadFuelCost.getText()));
		currentRoad.setCostPoll(Double.parseDouble(textRoadTollCost.getText()));

		currentRoad.setDestination(adjacencyCity);
		currentRoad.setOrigin(originCity);

		adjacencyCity.addAdjacency(currentRoad);
		originCity.addAdjacency(currentRoad);

		hideRoadDetails();
		if( flagAddRoad )
		{
		JOptionPane.showMessageDialog(frame,"Road Added!");
		}

		else
		{
			JOptionPane.showMessageDialog(frame,"Road Updated!");
		}
		tableRoad.repaint();

		flagAddRoad = false;
	}
	
	/* Cancel the road update (add or edit)
	 ----------------------------------------------------------------------------------------------*/
	private void cancelUpdateRoad() {
		
		if(flagAddRoad)
	{
		roads.remove(currentRoad);
		tableRoad.repaint();
	}
		
		hideRoadDetails();
		JOptionPane.showMessageDialog(frame,"Road data changed canceled, no chanegs will be made");
		flagAddRoad = false;
	}
	
	
	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
	   SEARCH (GRAPH)
	  /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/
	
	private void prepareSearchView() {
		for (City city : citiesController.getEntireList()) {
			if (city.hasWarehouse()) {
				cboxCityOrigin.addItem(city.getName());
			}
			if (city.hasRoad()) {
				cboxCityDestinantion.addItem(city.getName());
			}
		}
	}
	
	private void search() {
		String option = (String)cboxSearchCriteria.getSelectedItem();
		String citySrcName = (String)cboxCityOrigin.getSelectedItem();
		String cityDstName = (String)cboxCityDestinantion.getSelectedItem();

		if (option.equalsIgnoreCase(CRITERIA_SELECT_THE_CRITERIA)) {
			JOptionPane.showMessageDialog(frame,"Please, select a criteria");
			return;
		}
		City origin = citiesController.getByName(citySrcName);
		if (origin == null){
			JOptionPane.showMessageDialog(frame,"Please, select a valid origin");
			return;
		}
		City destination = citiesController.getByName(cityDstName);
		if (destination == null){
			JOptionPane.showMessageDialog(frame,"Please, select a valid destination");
			return;
		}

		AlgorithmWeight weightCriteria;
		if( option.equals(CRITERIA_TIME) ){
			weightCriteria = new AlgorithmWeightByTime();
		}else if( option.equals(CRITERIA_DISTANCE) ){
			weightCriteria = new AlgorithmWeightByDistance();
		}else {
			weightCriteria = new AlgorithmWeightByCost();
		}
		Algorithm algorithm = new Algorithm(cities);
		AlgorithmResult result = algorithm.shortestPath(origin, weightCriteria);
		ArrayList<City> pathToFollow = result.getPaths(destination);

		CityModel cityModel = new CityModel(pathToFollow);
		tableSearch.setModel(cityModel);
		tableSearch.repaint();
	}
}
