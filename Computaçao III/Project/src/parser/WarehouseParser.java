/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------
	Warehouse parser
*------------------------------------------------------------------------------------------------------------*/

package parser;

import java.util.ArrayList;

import core.CitiesController;
import core.City;
import core.Warehouse;
import csv.CSVHelper;

public class WarehouseParser {
	
	private CitiesController citiesController;
	
	/* Constructor 
	 *  must receive a CitiesController to get the city object according to the city id
	 ----------------------------------------------------------------------------------------------*/
	public WarehouseParser(CitiesController citiesController) {
		this.citiesController = citiesController;
	}

	/* ArrayList<String> and convert to ArrayList<Warehouse> returning this list
	 ----------------------------------------------------------------------------------------------*/
	public ArrayList<Warehouse> toWarehouseObjects(ArrayList<String> records){	
		ArrayList<Warehouse> warehouses = new ArrayList<Warehouse>();
		
		// ﻿WarehouseId,CityId,WarehouseName,CityName,HasRoad,Roads
		
		for (String record : records) {
		
			String[] elements = record.split(",");
			
			int warehouseId = Integer.parseInt(elements[0]);
			int cityId = Integer.parseInt(elements[1]);
			String warehouseName = elements[2];
			// String cityName = elements[3];
			
			City city = citiesController.getById(cityId);
			
			//city.getAdjacencies().add(e)
			
			Warehouse warehouse = new Warehouse(warehouseId, warehouseName, city);
			
			warehouses.add(warehouse);
		}
		
		return warehouses;
	}
	
	/* toCSVRecords
	   Convert a list of cities to a list of Strings (CSV struture)
	 ----------------------------------------------------------------------------------------------*/
	public static ArrayList<String> toCSVRecords(ArrayList<Warehouse> Warehouse){
		ArrayList<String> records = new ArrayList<String>();
		
		for(Warehouse warehouse : Warehouse) {
			City city = warehouse.getCity();
			String hasRoad = city.hasRoad() ? "Yes": "No";
			records.add(
				warehouse.getId() + CSVHelper.FIELD_SEPARATOR +
				city.getId() + CSVHelper.FIELD_SEPARATOR +
				warehouse.getName() + CSVHelper.FIELD_SEPARATOR +
				city.getName() + CSVHelper.FIELD_SEPARATOR +
				hasRoad + CSVHelper.FIELD_SEPARATOR +
				city.getNumberOfRoads()
			);
		}
		
		return records;
	}

}
