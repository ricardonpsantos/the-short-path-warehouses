/*------------------------------------------------------------------------------------------------------------
 	eFlow2W - Project of Computation III
 	
 	Group members:
 		- Cl�udia Sousa � r2014458; 
		- Jo�o Santos � r2016774; 
		- Jos� Sequeira - r2014489; 
		- Ricardo Santos - r2014517.

 		
*------------------------------------------------------------------------------------------------------------*/

package parser;

import java.util.ArrayList;

import core.CitiesController;
import core.City;
import core.Road;
import csv.CSVHelper;

public class RoadParser {
	
	private CitiesController citiesController;

	public RoadParser (CitiesController citiesController ) {
		this.citiesController = citiesController;
	}
	
	public ArrayList<Road> toRoadObjects(ArrayList<String> records){
		ArrayList<Road> roads= new ArrayList<Road>();

		for( String record : records) {
			String[] elements = record.split(",");

			int id = Integer.parseInt(elements[0]);
			char group = elements[1].charAt(0);
			int originId = Integer.parseInt(elements[2]);
			int destinationId = Integer.parseInt(elements[5]);
			double distance = Double.parseDouble(elements[7]);
			double time = Double.parseDouble(elements[8]);
			double costFuel = Double.parseDouble(elements[9]);
			double costPoll = Double.parseDouble(elements[10]); 
			double costDriver = Double.parseDouble(elements[11]);
			String period = elements[12];
			
			City origin = citiesController.getById(originId);
			City destination = citiesController.getById(destinationId);
			Road road= new Road(id, group, origin,  destination, distance, time, costFuel, costPoll, costDriver, period);
			
			origin.addRoad(road);
//			destination.addRoad(road);
			roads.add(road);
			origin.getAdjacencies().add(road);
		}
		return roads;
	}
	
	public static ArrayList<String> toCSVRecords(ArrayList<Road> roads){
		ArrayList<String> records = new ArrayList<String>();
		
		for(Road road : roads) {
			records.add(
				road.getRoadId() + CSVHelper.FIELD_SEPARATOR +
				road.getGroup() + CSVHelper.FIELD_SEPARATOR +
				road.getOrigin().getId() + CSVHelper.FIELD_SEPARATOR +
				road.getOrigin().getName() + CSVHelper.FIELD_SEPARATOR +
				road.getOrigin().getNumberOfRoads() + CSVHelper.FIELD_SEPARATOR +
				road.getDestination().getId() + CSVHelper.FIELD_SEPARATOR +
				road.getDestination().getName() + CSVHelper.FIELD_SEPARATOR +
				road.getDistance() + CSVHelper.FIELD_SEPARATOR +
				road.getTime() + CSVHelper.FIELD_SEPARATOR +
				road.getCostFuel() + CSVHelper.FIELD_SEPARATOR +
				road.getCostPoll() + CSVHelper.FIELD_SEPARATOR +
				road.getCostDriver() + CSVHelper.FIELD_SEPARATOR +
				road.getPeriod()
			);
		}
		return records;
	}
	
	

}
